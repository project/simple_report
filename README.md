## Overview

Module 'simple_report' is an API module which was designed to provide very
quick and simple way to build report using data stored in database table(s).

## Quick start

To get report you just need:

1. Create menu item using hook_menu().
2. Add menu callback. Eg.:
```
function <module_name>_report() {
  simple_report(['tables' => ['<table_name>']]);
}
```
3. Open created menu item in browser and you will see sortable and highly
customizable table with data from <table_name> database table and only selected
fields will be shown.


## Other options

You could extend your report with this features:

1. Automatically refresh page after given number of seconds. Disabled by default.
2. Join other tables to get more data.
3. Show/hide columns.
4. Set custom table headers. By default header will be build using fields
   names from database schema.
5. Custom formatter for each value in table.
6. Set number of items per page.
7. Use pager above or below table or both.
8. Use filters to show not all rows from selected database table.
   For example, to show only data related to given user at user's profile page.
9. Set custom page title.


## How to use

1. Create menu item using hook_menu().
2. Add menu callback which prepare data for simple_report().
3. Optionally add formatter for values in table.


### Example of menu callback

```
/**
 * Menu callback. Shows content of 'traffic_usage' table.
 *
 * @return string
 *   Returns rendered HTML table.
 */
function traffic_usage_report() {
  $table = 'traffic_usage';
  $meta_data = [
    // Set number of items per page.
    'items_per_page' => variable_get('traffic_usage_report_items_per_page', 10),
    // Page will be refreshed after given number of seconds.
    // Default is 0 which means autorefresh is disabled.
    'refresh_time' => variable_get('traffic_usage_report_refresh_time', 0),
    // Add custom labels for columns in header and make them sortable.
    'header' => [
      ['data' => t('Username'), 'field' => $table . '.username'],
      ['data' => t('Traffic'), 'field' => $table . '.traffic'],
      ['data' => t('Start Date'), 'field' => $table . '.start_date'],
      ['data' => t('End Date'), 'field' => $table . '.end_date'],
    ],
    // Required. Database tables which will be used in report.
    // At least base table must be specified.
    // If table has empty 'fields' element then all fields/columns will be used
    // from database table schema.
    'tables' => [
      // Table without 'join' element is the base table.
      $table => [
        // Optional. List of database table fields.
        // When 'fields' is empty or ommitted then all fields will be used.
        'fields' => ['username', 'traffic', 'start_date', 'end_date'],
      ],
      'field_data_field_login' => [
        // This table will be joined to base table.
        'join' => [
          // Join type could be: 'join', 'innerJoin', 'leftJoin', 'rightJoin'.
          // See https://www.drupal.org/docs/8/api/database-api/dynamic-queries/joins
          'type' => 'leftJoin',
          // Specify join condition.
          // WARNING: be sure to sanitize field value.
          'on' => 'traffic_usage.username = field_data_field_login.field_login_value',
        ],
        'fields' => ['entity_id'],
      ],
      'users' => [
        'join' => [
          'type' => 'leftJoin',
          'on' => 'users.uid = field_data_field_login.entity_id',
        ],

        'fields' => ['name', 'mail'],
      ],
    ],
    // Optional. Name of function to format values in report.
    // By default value will be sanitized by check_plain().
    //
    // Formatter function will get this parameters:
    // - $key - Required. Machine readable column name.
    // - $value - Value in this column to be processed.
    // - $row - Whole row with all the data. Not only data which will be shown
    //          but also hidden columns. Eg., columns from joined tables which
    //          has various IDs.
    'formatter' => 'traffic_usage_report_formatter',
    // Optional. Page Title.
    // Default title looks like this: 'Content of "<base_table_name>" table.
    'page_title' => 'Traffic Usage Report',
    // Show pager 'below', 'above' or 'both'.
    'pager' => 'both',
  ];
  return simple_report($meta_data);
}
```

### Example of field's value formatter

```
/**
 * Values formatter for 'simple_report'.
 *
 * @param string $name
 *   Required. Field name.
 *   Machine table field name (column name) without table name and dot.
 * @param mixed $value
 *   Field value.
 * @param array $row
 *   Data of the whole table row with hidden fields.
 *   This data could be used to format other fields. For example,
 *   hidden field 'uid' or 'entity_id' could be used to build a link to
 *   user profile page.
 *
 * @return mixed
 *   Returns formatted value.
 *   Empty value will not be formatted and returned as is.
 */
function traffic_usage_report_formatter(string $name, $value, array $row) {
  if (empty($value)) {
    return $value;
  }
  $name = check_plain($name);
  $value = check_plain($value);
  $date_format = variable_get('traffic_usage_report_date_format', 'short');

  $formatted = $value;
  switch ($name) {
    case 'username':
    case 'mail':
    case 'name':
      if (!empty($value) && !empty($row['entity_id'])) {
        $path = 'user/' . check_plain($row['entity_id']) . '/traffic-usage';
        $formatted = l(check_plain($value), $path);
      }
      $formatted = md5($value);
      break;

    case 'traffic':
      $formatted = example_convert_to_human_readable($value);
      break;

    case 'start_date':
    case 'end_date':
      $formatted = example_format_date($value, $date_format);
      break;

    default:
      // It's not necessary.
      $formatted = check_plain($value);
      break;
  }
  return $formatted;
}
```


## Example of hook_menu() and hook_permission()

```
/**
 * Implements hook_permission().
 */
function traffic_usage_permission() {
  return [
    'administer traffic_usage' => [
      'title' => t('Administer "traffic_usage" module'),
      'description' => t('Allows to change all settings.'),
    ],
    'access traffic_usage reports' => [
      'title' => t('Access "traffic_usage" reports'),
      'description' => t('Allows to review all reports (general and user).'),
    ],
  ];
}

/**
 * Implements hook_menu().
 */
function traffic_usage_menu() {
  $items = [];
  $items['user/%user/traffic-usage'] = [
    'title' => 'Traffic',
    'description' => "User's traffic",
    'page callback' => 'traffic_usage_user_report',
    'page arguments' => [1],
    'access arguments' => ['access traffic_usage reports'],
    'type' => MENU_LOCAL_TASK,
    'file' => 'traffic_usage.pages.inc',
  ];
  $items['admin/reports/traffic-usage'] = [
    'title' => 'Traffic Usage Report',
    'description' => "All User's Traffic Usage Report",
    'page callback' => 'traffic_usage_report',
    'access arguments' => ['administer traffic_usage'],
    'type' => MENU_LOCAL_TASK,
    'file' => 'traffic_usage.report.inc',
  ];
  return $items;
}
```

## Example of filters usage

```
/**
 * Menu callback. Shows user's traffic report.
 *
 * @param int $account
 *   Required. Fully loaded user account.
 */
function traffic_usage_user_report($account) {
  if (empty($account)) {
    return;
  }
  $table = 'traffic_usage';
  $meta_data = [
    'items_per_page' => variable_get('traffic_usage_report_items_per_page', 10),
    'refresh_time' => variable_get('traffic_usage_report_refresh_time', 0),
    'header' => [
      ['data' => t('Username'), 'field' => $table . '.username'],
      ['data' => t('Traffic'), 'field' => $table . '.traffic'],
      ['data' => t('Start Date'), 'field' => $table . '.start_date'],
      ['data' => t('End Date'), 'field' => $table . '.end_date'],
    ],
    'tables' => [
      $table => [],
      'field_data_field_login' => [
        'join' => [
          'type' => 'leftJoin',
          'on' => 'traffic_usage.username = field_data_field_login.field_login_value',
        ],
        'fields' => ['entity_id'],
      ],
      'users' => [
        'join' => [
          'type' => 'leftJoin',
          'on' => 'users.uid = field_data_field_login.entity_id',
        ],

        'fields' => ['name', 'mail'],
      ],
    ],
    'formatter' => 'traffic_usage_report_formatter',
    'page_title' => 'Traffic Usage Report',
    'pager' => 'both',
  ];
  $filter[] = [
    'name' => 'username',
    'value' => '<username value>',
  ];
  return simple_report($meta_data, $filter);
}
```
